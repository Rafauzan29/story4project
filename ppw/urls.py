from django.urls import include, path
from .views import *

urlpatterns = [
        path('', index, name = 'index'),
        path('visimisi/', visimisi, name = 'visimisi'),
        path('story1/', story1, name = 'story1'),
        path('profile-1/', profile1, name = 'profile1'),
        path('profile-2/', profile2, name = 'profile2'),
        path('profile-3/', profile3, name = 'profile3'),
        path('profile-4/', profile4, name = 'profile4'),
        path('profile-5/', profile5, name = 'profile5'),
        path('profile-6/', profile6, name = 'profile6'),
        path('profile-7/', profile7, name = 'profile7'),
        path('profile-8/', profile8, name = 'profile8'),
        path('profile-9/', profile9, name = 'profile9'),
    ]
